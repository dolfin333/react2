import { createStore } from "redux";
import chatReducer from "./store/chat/reducer"

export default createStore(chatReducer);