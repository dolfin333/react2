import React from "react";
import { connect } from "react-redux";
import "./Chat.css";
import Preloader from "./preloader/Preloader";
import MessageInput from "./message_input/MessageInput";
import Header from "./header/Header";
import MessageList from "./message_list/MessageList";
import Modal from "./modal/Modal";
import * as actions from "../store/chat/actions";

class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.addNewMessage = this.addNewMessage.bind(this);
    this.updateMessage = this.updateMessage.bind(this);
    this.setInputText = this.setInputText.bind(this);
    this.cancelUpdate = this.cancelUpdate.bind(this);
    this.deleteMessage = this.deleteMessage.bind(this);
    this.editMessage = this.editMessage.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
  }

  async componentDidMount() {
    const messages = await (
      await fetch(this.props.url, { method: "GET" })
    ).json();
    const usersMap = new Map();
    messages.forEach((message) => {
      usersMap.set(
        message.user,
        typeof usersMap.get(message.user) !== "undefined"
          ? 1 + usersMap.get(message.user)
          : 1
      );
    });
    messages.sort((a, b) => {
      return new Date(a.createdAt) - new Date(b.createdAt);
    });
    this.props.setStartState({
      messages,
      preloader: false,
      lastMessageDate: messages[messages.length - 1]
        ? messages[messages.length - 1].createdAt
        : "",
      messageCounter: messages.length,
      usersCounter: usersMap.size,
      usersMap: usersMap,
      ownUserId: "9e243930-83c9-11e9-8e0c-8f1a686f4ce5",
      nextId: 1,
      editModal: false,
    });
  }

  setInputText(text) {
    this.props.setInputText(text);
  }

  addNewMessage(text) {
    this.props.addNewMessage(text);
  }
  deleteMessage(id) {
    this.props.deleteMessage(id);
  }

  editMessage(id) {
    this.props.editMessage(id);
    this.props.showModal();
  }

  cancelUpdate() {
    this.props.hideModal();
    this.props.cancelUpdate();
  }

  updateMessage(id, text) {
    this.props.hideModal();
    this.props.updateMessage(id, text);
  }

  getLastUserMessage(messages, userId) {
    const filteredMessages = messages.filter(
      (message) => message.userId === userId
    );
    if (filteredMessages.length !== 0) {
      return filteredMessages[filteredMessages.length - 1];
    }
    return null;
  }

  handleKeyDown(e) {
    if (e.code === "ArrowUp") {
      const lastMessage = this.getLastUserMessage(
        this.props.chat.messages,
        this.props.chat.ownUserId
      );
      if (lastMessage) {
        this.props.editMessage(lastMessage.id);
        this.props.showModal();
      }
    }
  }
  render() {
    return (
      <div className="chat" tabIndex="1" onKeyDown={this.handleKeyDown}>
        {this.props.chat.preloader ? (
          <Preloader />
        ) : (
          <div className="chat-main-content">
            <Header
              messageCounter={this.props.chat.messageCounter}
              usersCounter={this.props.chat.usersCounter}
              lastMessageDate={this.props.chat.lastMessageDate}
            />
            <MessageList
              messages={this.props.chat.messages}
              ownUserId={this.props.chat.ownUserId}
              deleteMessage={this.deleteMessage}
              editMessage={this.editMessage}
            />
          </div>
        )}
        <MessageInput
          addNewMessage={this.addNewMessage}
          setInputText={this.setInputText}
          textInput={this.props.chat.textInput}
        />
        {this.props.chat.editModal && (
          <Modal
            isShow={this.props.chat.editModal}
            editedMessage={this.props.chat.editedMessage}
            cancelUpdate={this.cancelUpdate}
            updateMessage={this.updateMessage}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    chat: state.chat,
  };
};
const mapDispatchToProps = {
  ...actions,
};
export default connect(mapStateToProps, mapDispatchToProps)(Chat);
