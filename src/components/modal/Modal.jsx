import React from "react";
import "./Modal.css";

class Modal extends React.Component {
  constructor(props) {
    super(props);
    this.state = { text: "" };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (prevState.text === "") {
      return { text: nextProps.editedMessage.text };
    }
    return prevState;
  }
  render() {
    return (
      <div
        className={
          this.props.isShow
            ? "edit-message-modal modal-shown"
            : "edit-message-modal"
        }
      >
        <div className="modal-content">
          <div className="modal-title">Edit message</div>
          <input
            type="text"
            className="edit-message-input"
            value={this.state.text}
            onChange={(e) => {
              this.setState({ text: e.target.value });
            }}
          />
          <div className="modal-buttons">
            <button
              className="edit-message-button"
              onClick={() =>
                this.props.updateMessage(
                  this.props.editedMessage.id,
                  this.state.text
                )
              }
            >
              Edit
            </button>
            <button
              className="edit-message-close"
              onClick={() => this.props.cancelUpdate()}
            >
              Close
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default Modal;
