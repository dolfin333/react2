import moment from "moment";
import React from "react";
import "./Header.css";

class Header extends React.Component {
  getDateFormat(lastMessageDate) {
    if (lastMessageDate !== "") {
      return moment(lastMessageDate).format("DD.MM.YYYY HH:mm");
    }
    return "";
  }
  render() {
    return (
      <div className="header">
        <div className="header-left-content">
          <div className="header-title">Sweety chat</div>
          <div className="header-users-count-wrap">
            <span>Users</span>
            <div className="header-users-count">{this.props.usersCounter}</div>
          </div>
          <div className="header-messages-count-wrap">
            <span>Messages</span>
            <div className="header-messages-count">
              {this.props.messageCounter}
            </div>
          </div>
        </div>
        <div className="header-last-message-date-wrap">
          <span>Last message</span>
          <div className="header-last-message-date">
            {this.getDateFormat(this.props.lastMessageDate)}
          </div>
        </div>
      </div>
    );
  }
}

export default Header;
