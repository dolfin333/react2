import moment from "moment";
import React from "react";
import Message from "../message/Message";
import OwnMessage from "../own_message/OwnMessage";
import "./MessageList.css";

class MessageList extends React.Component {
  groupByDay(array) {
    const mapValuesByDate = new Map();
    const copyArray = array.slice(0);
    copyArray.forEach((value) => {
      let d = moment(new Date(value.createdAt)).format("l");
      if (typeof mapValuesByDate.get(d) !== "undefined") {
        mapValuesByDate.get(d).push(value);
      } else {
        mapValuesByDate.set(d, [value]);
      }
    });
    return mapValuesByDate;
  }

  getDifferenceInDays(date1, date2) {
    const days = Math.floor(
      (date2.getTime() - date1.getTime()) / (1000 * 3600 * 24)
    );
    return days;
  }
  getDate(date1, date2) {
    const diffDays = this.getDifferenceInDays(date1, date2);
    if (diffDays === 0) {
      return "Today";
    }
    if (diffDays === 1) {
      return "Yesterday";
    }
    return moment(date1).format("dddd, DD MMMM");
  }
  render() {
    const messagesMap = this.groupByDay(this.props.messages);
    return (
      <div className="message-list">
        {Array.from(messagesMap.entries()).map(([key, value]) => (
          <div key={value[0].id + value[0].userId}>
            <div className="messages-divider">
              <div className="messages-divider-date">
                {this.getDate(new Date(key), new Date())}
              </div>
            </div>
            {value.map((message) =>
              message.userId === this.props.ownUserId ? (
                <OwnMessage
                  message={message}
                  key={message.id}
                  deleteMessage={this.props.deleteMessage}
                  editMessage={this.props.editMessage}
                />
              ) : (
                <Message message={message} key={message.id} />
              )
            )}
          </div>
        ))}
      </div>
    );
  }
}

export default MessageList;
