import moment from "moment";
import React from "react";
import "./OwnMessage.css";

class OwnMessage extends React.Component {
  getFormatedDate(message) {
    const dateOb = message.editedAt
      ? new Date(message.editedAt)
      : new Date(message.createdAt);
    return moment(dateOb).format("HH:mm");
  }
  render() {
    return (
      <div className="own-message-wrap">
        <div className="own-message">
        <div className="buttons">
            <button
              className="message-edit"
              onClick={() => {
                this.props.editMessage(this.props.message.id);
              }}
            >
              edit
            </button>
            <button
              className="message-delete"
              onClick={() => {
                this.props.deleteMessage(this.props.message.id);
              }}
            >
              delete
            </button>
          </div>
          <div className="own-message-content">
            <div className="message-text">{this.props.message.text}</div>
            <div className="message-time">
              {this.props.message.editedAt && "edited at "}
              {this.getFormatedDate(this.props.message)}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default OwnMessage;
