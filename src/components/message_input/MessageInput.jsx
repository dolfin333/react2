import React from "react";
import "./MessageInput.css";

class MessageInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = { text: "" };
  }
  render() {
    return (
      <div className="message-input">
        <div className="message-input-content">
          <input
            value={this.state.text}
            className="message-input-text"
            onChange={(e) => {
              this.setState({ text: e.target.value });
            }}
          />
          <button
            className="message-input-button"
            onClick={() => {
              if (this.state.text !== "") {
                this.props.addNewMessage(this.state.text);
                this.setState({ text: "" });
              }
            }}
          >
            Send
          </button>
        </div>
      </div>
    );
  }
}

export default MessageInput;
