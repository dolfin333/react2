/* eslint-disable import/no-anonymous-default-export */
import {
  ADD_NEW_MESSAGE,
  DELETE_MESSAGE,
  EDIT_MESSAGE,
  CANCEL_UPDATE,
  UPDATE_MESSAGE,
  SET_START_STATE,
  SHOW_MODAL,
  HIDE_MODAL,
  SHOW_PRELOAD,
  HIDE_PRELOAD,
} from "./actionTypes";

const initialState = {
  chat: {
    messages: [],
    editModal: false,
    preloader: true,
    lastMessageDate: null,
    messageCounter: null,
    usersCounter: null,
    usersMap: null,
    ownUserId: null,
    nextId: null,
    editedMessage: null,
    updateTextInput: "",
  },
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SHOW_PRELOAD: {
      return {
        chat: {
          ...state.chat,
          preloader: true,
        },
      };
    }
    case HIDE_PRELOAD: {
      return {
        chat: {
          ...state.chat,
          preloader: false,
        },
      };
    }
    case SHOW_MODAL: {
      return {
        chat: {
          ...state.chat,
          editModal: true,
        },
      };
    }
    case HIDE_MODAL: {
      return {
        chat: {
          ...state.chat,
          editModal: false,
        },
      };
    }
    case SET_START_STATE: {
      const { chat } = action.payload;
      return {
        chat: {
          ...state.chat,
          ...chat,
        },
      };
    }
    case ADD_NEW_MESSAGE: {
      const { text } = action.payload;
      const message = {
        id: state.chat.nextId,
        userId: state.chat.ownUserId,
        avatar:
          "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA",
        user: "Liza",
        text: text,
        createdAt: new Date(),
        editedAt: "",
      };
      const usersMap = state.chat.usersMap;
      const count = usersMap.get(message.user);
      usersMap.set(message.user, typeof count !== "undefined" ? 1 + count : 1);
      const messages = state.chat.messages;
      messages.push(message);
      return {
        chat: {
          ...state.chat,
          messages: messages,
          nextId: state.chat.nextId + 1,
          text_input: "",
          usersCounter: usersMap.size,
          usersMap: usersMap,
          messageCounter: state.chat.messageCounter + 1,
          lastMessageDate: message.createdAt,
        },
      };
    }
    case DELETE_MESSAGE: {
      const { id } = action.payload;
      const messages = state.chat.messages;
      const index = messages.findIndex((mess) => mess.id === id);
      const usersMap = state.chat.usersMap;
      usersMap.set(
        messages[index].user,
        usersMap.get(messages[index].user) - 1
      );
      if (usersMap.get(messages[index].user) === 0) {
        usersMap.delete(messages[index].user);
      }
      messages.splice(index, 1);
      return {
        chat: {
          ...state.chat,
          messages: messages,
          messageCounter: state.chat.messageCounter - 1,
          usersCounter: usersMap.size,
          usersMap: usersMap,
          lastMessageDate: messages[messages.length - 1]
            ? messages[messages.length - 1].createdAt
            : "",
        },
      };
    }
    case EDIT_MESSAGE: {
      const { id } = action.payload;
      const messages = state.chat.messages;
      return {
        chat: {
          ...state.chat,
          editedMessage: messages.find((mess) => mess.id === id),
        },
      };
    }
    case CANCEL_UPDATE: {
      return {
        chat: {
          ...state.chat,
          editedMessage: null,
        },
      };
    }
    case UPDATE_MESSAGE: {
      const { id, text } = action.payload;
      const messages = state.chat.messages;
      const messageId = messages.findIndex((mess) => mess.id === id);
      if (messages[messageId].text !== text) {
        messages[messageId].text = text;
        messages[messageId].editedAt = new Date();
      }
      return {
        chat: {
          ...state.chat,
          messages: messages,
          editedMessage: null,
        },
      };
    }
    default: {
      return state;
    }
  }
}
