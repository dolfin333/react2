import {
  ADD_NEW_MESSAGE,
  DELETE_MESSAGE,
  EDIT_MESSAGE,
  CANCEL_UPDATE,
  UPDATE_MESSAGE,
  SET_START_STATE,
  SHOW_MODAL,
  HIDE_MODAL,
  SHOW_PRELOAD,
  HIDE_PRELOAD,
} from "./actionTypes";

export const showPreload = () => ({
  type: SHOW_PRELOAD,
});

export const hidePreload = () => ({
  type: HIDE_PRELOAD,
});

export const showModal = () => ({
  type: SHOW_MODAL,
});

export const hideModal = () => ({
  type: HIDE_MODAL,
});

export const setStartState = (chat) => ({
  type: SET_START_STATE,
  payload: {
    chat,
  },
});
export const addNewMessage = (text) => ({
  type: ADD_NEW_MESSAGE,
  payload: {
    text,
  },
});
export const deleteMessage = (id) => ({
  type: DELETE_MESSAGE,
  payload: {
    id,
  },
});
export const editMessage = (id) => ({
  type: EDIT_MESSAGE,
  payload: {
    id,
  },
});
export const cancelUpdate = () => ({
  type: CANCEL_UPDATE,
});
export const updateMessage = (id, text) => ({
  type: UPDATE_MESSAGE,
  payload: {
    id,
    text,
  },
});
