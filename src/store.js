import { createStore } from "redux";
import chatReducer from "./store/chat/reducer.js";

export default createStore(chatReducer);
