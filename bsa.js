import Chat from "./src/components/Chat";
import rootReducer from "./src/store/chat/reducer";

export default { Chat, rootReducer };
